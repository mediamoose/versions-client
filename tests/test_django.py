from __future__ import unicode_literals

from base64 import b64encode

from django.test import SimpleTestCase, override_settings


class VersionsTestCase(SimpleTestCase):

    @override_settings(VERSIONS_AUTH=False)
    def test_auth_disabled(self):
        resp = self.client.get('/versionz')
        self._test_response(resp)

    @override_settings(VERSIONS_AUTH=[['mediamoose', 'secret']])
    def test_auth_list(self):
        resp = self.client.get('/versionz')
        assert resp.status_code == 401
        assert resp.content == b'401 unauthorized'
        auth = 'Basic ' + b64encode(b'mediamoose:secret').decode('utf-8')
        resp = self.client.get('/versionz', HTTP_AUTHORIZATION=auth)
        self._test_response(resp)

    @override_settings(VERSIONS_AUTH=[('mediamoose', 'secret')])
    def test_auth_tuple(self):
        resp = self.client.get('/versionz')
        assert resp.status_code == 401
        assert resp.content == b'401 unauthorized'
        auth = 'Basic ' + b64encode(b'mediamoose:secret').decode('utf-8')
        resp = self.client.get('/versionz', HTTP_AUTHORIZATION=auth)
        self._test_response(resp)

    @override_settings(VERSIONS_AUTH=['mediamoose:secret'])
    def test_auth_string(self):
        resp = self.client.get('/versionz')
        assert resp.status_code == 401
        assert resp.content == b'401 unauthorized'
        auth = 'Basic ' + b64encode(b'mediamoose:secret').decode('utf-8')
        resp = self.client.get('/versionz', HTTP_AUTHORIZATION=auth)
        self._test_response(resp)

    @override_settings(VERSIONS_AUTH=[('mediamoose', 'secret')])
    def test_realm(self):
        resp = self.client.get('/versionz')
        assert resp['WWW-Authenticate'] == 'Basic realm="Private section"'
        with override_settings(VERSIONS_REALM='Hello world!'):
            resp = self.client.get('/versionz')
            assert resp['WWW-Authenticate'] == 'Basic realm="Hello world!"'

    def _test_response(self, resp):
        assert resp.status_code == 200
        assert b'version_application{version="1.0"}' in resp.content
