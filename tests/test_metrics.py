from __future__ import unicode_literals

from os import environ

from django.test import SimpleTestCase

import versions_client


class VersionsTestCase(SimpleTestCase):

    def test_generate_versions(self):
        versions = versions_client.generate_versions(version='1.0').decode(
            'utf-8')

        assert '# HELP version_application version info.' in versions
        assert '# TYPE version_application gauge' in versions
        assert 'version_application{version="1.0"}' in versions

        assert '# HELP version_platform version info.' in versions
        assert '# TYPE version_platform gauge' in versions
        assert 'version_platform{' in versions
        assert 'distro_name="{}"'.format(
            environ.get('TEST_DISTRO_NAME')) in versions
        assert 'distro_id="{}"'.format(
            environ.get('TEST_DISTRO_ID')) in versions
        assert 'distro_version="{}'.format(
            environ.get('VERSION_ID')) in versions, versions

        assert '# HELP version_python version info.' in versions
        assert '# TYPE version_python gauge' in versions
        assert 'version_python{' in versions

        assert '# HELP version_package version info.' in versions
        assert '# TYPE version_package gauge' in versions, versions
        assert 'version_package{group="os",' in versions, versions
        assert 'version_package{group="python",name="versions-client",' \
               'version="-VERSION-"} 1' in versions, versions

        # Prometheus requires metrics to end with a newline
        assert versions[-1] == '\n'
