from __future__ import unicode_literals

from .utils import generate_versions  # noqa


__version__ = '{VERSION}'
