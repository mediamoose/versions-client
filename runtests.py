#!/usr/bin/env python

import sys

from django.conf import settings
from django.test.utils import get_runner


settings.configure(
    ROOT_URLCONF='versions_client.django.urls',
    VERSIONS_LABELS={
        'version': '1.0'
    }
)

try:
    import django
    setup = django.setup
except AttributeError:
    pass
else:
    setup()


def run_tests(*test_args):
    if not test_args:
        test_args = ['tests']

    # Run tests
    TestRunner = get_runner(settings)
    test_runner = TestRunner()

    failures = test_runner.run_tests(test_args)

    if failures:
        sys.exit(bool(failures))


if __name__ == '__main__':
    run_tests(*sys.argv[1:])
